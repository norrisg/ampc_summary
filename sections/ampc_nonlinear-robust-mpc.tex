\section{NONLINEAR ROBUST MPC}

\begin{whitebox}{\textbf{LINEAR ROBUST MPC}}
    \blue{System} 
    \begin{whitebox}{}
        \footnotesize 
        $x(k+1) = Ax(k) + Bu(k) + w(k) \quad x(k)\in\X, u(k)\in\U, w(k)\in\W$
    \end{whitebox}

    \blue{Idea} 
    \begin{highlightbox}{}
        \footnotesize
        \textbf{Seperate true sys $(x,u)$ into nom sys $(z,v)$ and error $e=x-z$}
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Drive nominal system to origin $\qquad z(k+1) = Az(k) + Bv(k)$
            \item FB to ensure real traj \emph{tracks} nom $\quad u(k) = K(x(k)-z(k)) + v(k)$
        \end{enumerate}
        \textbf{Only opt \& predict over nominal traj} $\leadsto$ complexity similar to nom MPC
    \end{highlightbox}

    \blue{Error Dynamics} 
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Error Dynamics $\qquad e_{i+1} = (A+BK) e_i + w_i = A_K e_i + w_i$
            \item $A_K$ Schur stable and $\W$ compact
            \item \textbf{DRS} -- Disturbance reacable set 
            \begin{align*}
                \F_{i+1} = A_K \F_i \oplus \W, \F_0 = \{0\} 
                \quad \Rightarrow e_i \in \F_i = \textstyle\bigoplus_{j=1}^{i-1} A_K^j \W \text{ if } e_0=0
            \end{align*}
            \item \textbf{RPI} set $\E$ s.t. $e_i \in \E \quad \Rightarrow e_{i+1} \in \E$
        \end{itemize}
    \end{whitebox}

    \tcbsubtitle{\textbf{CONSTRAINT-TIGHTENING MPC}}

    \blue{Constraint-Tightening MPC}
    \begin{whitebox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            V_N^\star(x) = \min_V \ & \textstyle\sum_{i=0}^{N-1} l(z_i, v_i) + l_f(z_N) \\
            \mathrm{s.t.} \ &z_{i+1} = Az_i + Bv_i, \qquad i=0,\dots,N-1 \\
            &z_i \in \bar{\X}_i = {\color{color2}\X \ominus \F_i}, \qquad i=0,\dots,N-1 \\
            &v_i \in \bar{\U}_i = {\color{color2}\U \ominus K\F_i}, \qquad i=0,\dots,N-1 \\
            &z_N \in \X_f \ominus \F_N, {\color{color2}\qquad z_0 = x}
        \end{align*}

        \begin{minipage}[c]{0.55\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{DRS} $\F_i$ (offline)
                \item \textbf{Control Law:} $\mu_{\mathrm{tight}}(x) = v_0^\star(x)$
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.43\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{Terminal Set:} \\
                $\X_f =$ (maximal) RPI set
            \end{itemize}
        \end{minipage}
    \end{whitebox}

    \blue{Robust Stability}
    \begin{whitebox}{}
        \footnotesize 
        Assume $l_f, \X_f, \pi_f$ designed well, 
        cost $l, lf$ uniformly cont, 
        feasible at $k=0$ with IC $x(0)$

        Then constraint-tightening MPC \textbf{recursively feasible}, 
        CL system
        $Ax(k) + B\mu_{\mathrm{tight}}(x(k)) +  w(k)$
        is ISS on feasible set $\X_N$ wrt $w(k)$
    \end{whitebox}

    \blue{Properties} $\leadsto$ \textbf{tube-MPC} for stronger stability properties
    \begin{highlightbox}{}
        \footnotesize
        \begin{minipage}[c]{0.45\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item Robust constraint satisfaction 
                \item Clearly defined ROA
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.53\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item Only qualitative stability properties (ISS, $V_N^\star$ as ISS-Lyap function)
            \end{itemize}
        \end{minipage}
    \end{highlightbox}

    \tcbsubtitle{\textbf{LINEAR TUBE-MPC}}

    \blue{Linear Tube-MPC}
    \begin{whitebox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            V_N^\star(x) = \min_{V, z_0} \ & \textstyle\sum_{i=0}^{N-1} l(z_i, v_i) + l_f(z_N) \\
            \mathrm{s.t.} \ &z_{i+1} = Az_i + Bv_i, \qquad i=0,\dots,N-1 \\
            &z_i \in \bar{\X} = {\color{color2}\X \ominus \E}, \qquad i=0,\dots,N-1 \\
            &v_i \in \bar{\U} = {\color{color2}\U \ominus K\E}, \qquad i=0,\dots,N-1 \\
            &z_N \in \X_f, \qquad {\color{color2}x \in \{z_0\}\oplus \E}
        \end{align*}

        \begin{minipage}[c]{0.55\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{RPI Set:} $\E$ (offline)
                \item \textbf{Initial Constraint:} $x \in z_0 \oplus \E$
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.43\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{Control Law:} $\mu_{\mathrm{tube}}(x) = K(x-z_0^\star(x)) + v_0^\star(x)$
            \end{itemize}
        \end{minipage}
    \end{whitebox}

    \blue{Robust Stability}
    \begin{whitebox}{}
        \footnotesize 
        Assume $l_f, \X_f, \pi_f$ designed well, 
        feasible at $k=0$ with IC $x(0)$

        Then tube-MPC \textbf{recursively feasible}, and $x(k)$ converges to set $\E$ for CL system
        $\qquad Ax(k) + B\mu_{\mathrm{tube}}(x(k)) +  w(k)$
    \end{whitebox}

    \blue{Properties} 
    \begin{highlightbox}{}
        \footnotesize
        \begin{minipage}[c]{0.45\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item Robust constraint satisfaction 
                \item Clearly defined ROA
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.53\linewidth}
            \begin{itemize}[leftmargin=1em]
                \item Strong conv./stab. guarantees
                \item Smaller feasible set
            \end{itemize}
        \end{minipage}

        \vspace{0.25em}
        \begin{itemize}[leftmargin=1em]
            \item RPI set $\E$ increases offline complexity and online implementation
        \end{itemize}
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{NONLINEAR ERROR DYNAMICS}}
    \blue{System}
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            x(k+1) = f(x(k), u(k)) + w(k), \quad (x, u) \in \mathcal{Z} \subseteq \X \times \U, \quad w \in\W
        \end{align*}
    \end{whitebox}
    \blue{Goal} extend linear tube-MPC to nonlinear setting \\
    $\leadsto$ use nomminal system $(z,v)$, try to find RPI set $\E$ for error

    \blue{Error Dynamics}
    \begin{highlightbox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            e_{i+1} &= f(z_i + e_i, \kappa(z_i + e_i, z_i, v_i)) - f(z_I, v_i) + w \\
            &=: g(e_i, z_i, v_i) + w_i \neq g(e_i) + w_i
        \end{align*}
        \textbf{ACHTUNG} dynamics depend on nominal state and input! \\
        $\leadsto$ \textbf{exact disturbance propagation difficult for nonlinear systems}
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{INCREMENTAL ISS \& STABILITY}}
    \blue{Incremental Asymptotic Stability} 
    \begin{highlightbox}{}
        \footnotesize
        \textbf{Intuition} not thinking about whether some point stable instead about trajectories converging to each other
        \begin{align*}
            \| x(k) - z(k) \| &\leq \beta(\|x_0-z_0\|, k), \quad \beta \in \KL \\
            z(k+1) &= f(z(k), u(k)), \quad z(0) = z_0 \\
            x(k+1) &= f(x(k), u(k)), \quad x(0) = x_0
        \end{align*}
        \textbf{Difference between 2 dynamics should be bounded by $\KL$ function}
    \end{highlightbox}

    \blue{Incremental Lyap Function} $V_\delta(x,z)$ with exp. decay 
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            \alpha_1(\|x-z\|) \leq V_\delta(x,z) &\leq \alpha_2(\|x-z\|), \quad \alpha_{\{1,2\}} \in \K_\infty \\
            V_\delta(f(x,u),f(z,u)) &\leq \rho V_\delta(x,z), \quad \rho \in [0,1)
        \end{align*}
    \end{whitebox}

    \blue{Incremental Stabilizability} $\exists$ stabilizing FB $\kappa$ s.t.
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            V_\delta(f(x,\kappa(x,z,v)), f(z,v)) \leq \rho V_\delta(x,z), \quad \rho \in [0,1)
        \end{align*}
    \end{whitebox}
    \blue{Incremental ISS} 
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            V_\delta(f(x,\kappa(x,z,v)){\color{color2}+w}, f(z,v)) &\leq \rho V_\delta(x,z) {\color{color2}+ \alpha_w(\|w\|)}, \\
            \quad \rho \in [0,1), \quad &\alpha_w \in \K
        \end{align*}

        \textbf{Intuition:} true uncertain sys $\approx$ nom sys if $w$ bounded and FB applied
    \end{whitebox}
\end{whitebox}
    
\begin{whitebox}{\textbf{RPI SETS WITH INCREMENTAL STABILITY}}

    \blue{NL Tube-MPC RPI Set Ansatz}
    \begin{whitebox}{}
        \footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item \textbf{i-ISS} true uncertain sys $\approx$ nominal sys if $w$ bounded, FB $\kappa$ applied \\
            \item if have such a Lyap function $V_\delta$ $\leadsto$ RPI set:
            \begin{align*}
                {\color{color2}\Omega} := \{ (x,z) \mid V_\delta(x,z) \leq \delta \}
            \end{align*}
            sublevel sets of lyap func are invariant $\leadsto$ need to choose level s.t RPI
            \item \textbf{RPI Condition}
            \begin{align*}
                (f(x,\kappa(x,z,v)+w), f(z,v)) \in {\color{color2}\Omega}, \quad \forall w \in \W, (x,z)\in\Omega
            \end{align*}
            \item \textbf{i-ISS Condition} with $x^+ = f(x,\kappa(x,z,v))+w, z^+ = f(z,v)$
            \begin{align*}
                V_\delta(x^+, z^+) \leq \rho V_\delta(x,z) + \alpha_w(\|w\|) \leq {\color{color2}\rho\delta + \bar{w}\overset{!}{\leq} \delta} \\
                \Rightarrow {\color{color2} \delta = \textstyle\frac{\bar{w}}{1-\rho}} \quad \text{(for equality)}
                \qquad \bar{w} := \max_{w\in\W} \alpha_w(\|w\|)
            \end{align*}
        \end{itemize}
    \end{whitebox}

    \tcbsubtitle{\textbf{INCREMENTAL LYAP FUNC -- OFFLINE DESIGN}}
    {\footnotesize
    \begin{itemize}[leftmargin=1em]
        \item Simplest case -- quadratic incremental Lyap function with linear feedback 
        \item Define differential dynamics/linearization
        \item Gradient theorem 
        $\Rightarrow$ $\delta$-ISS condition 
        \item Matrices $M,K$ can be designed using LMIs
    \end{itemize}
    }
    \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {34},
            trim = {10mm, 5mm, 10mm, 10mm},
            clip
            ]{03_robustNMPC.pdf}
        }
        
\end{whitebox}

\begin{whitebox}{\textbf{NONLINEAR TUBE-MPC}}
    \textbf{Difficulty was getting RPI set, now $\approx$ linear tube-MPC}

    \begin{minipage}[c]{0.48\linewidth}
        \blue{Tube Based Robust MPC}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {39},
            trim = {25mm, 5mm, 35mm, 10mm},
            clip
            ]{03_robustNMPC.pdf}
        }
    \end{minipage}
    \begin{minipage}[c]{0.5\linewidth}
        \begin{whitebox}{}
            \footnotesize 
            \vspace{-1em}
            \begin{align*}
                \min_{V, z_0} \ & \textstyle\sum_{i=0}^{N-1} l(z_i, v_i) + l_f(z_N) \\
                \mathrm{s.t.} \ &z_{i+1} = Az_i + Bv_i \\
                &{\color{color2}(z_i, v_i) \in \bar{\mathcal{Z}}}, \quad z_N \in \X_f \\
                &{\color{color2}(x,z_0) \in \Omega}
            \end{align*}

            \begin{itemize}[leftmargin=1em]
                \item \textbf{Tight. Nom Constraints} $\bar{\mathcal{Z}} \subseteq \mathcal{Z}$
                \item \textbf{Initial Constraint:} $(x,z_0) \in \Omega$
                \item \textbf{Control Law:} $\mu_{\mathrm{tube}}(x) = \kappa(x, z_0^\star(x), v_0^\star(x))$
            \end{itemize}
        \end{whitebox}
    \end{minipage}

    \blue{Assumptions}
    \begin{whitebox}{}
        \footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item RPI set $\Omega$ from incremental Lyap func $V_\delta$
            \item Tightened constraint set 
            \item Terminal set: $\forall x\in\X_f$ we have \textbf{pos invarariance, constraint satisfaction, local lyap function}
            \item Positive definite, bounded cost 
            \begin{itemize}[leftmargin=1em]
                \item $l(x,u) \geq \alpha_l(\|x\|), \quad \forall x\in\X, u\in\U, \alpha_l \in \K_\infty$
                \item $0\in \mathrm{int}(\X_f)$ and $l_f(x) \leq \alpha_f(\|x\|), \forall x \in \X_f, \alpha_f \in \K_\infty$
            \end{itemize}
        \end{itemize}
    \end{whitebox}

    \blue{Robust Invariance \& Robust Stability}
    \begin{whitebox}{}
        \footnotesize 
        Let assumptions hold, problem feasible at $k=0$ for $x(0)$ \\
        \textbf{Then:}
        \begin{itemize}[leftmargin=1em]
            \item nonlinear tube-MPC recursively feasible \& constraints satisfied $\forall k\geq0$ 
            for CL sys $f(x(k), \mu_{\mathrm{tube}}(x(k)))+w(k)$
            \item CL converges to RPI set $\Omega_x = \{ x \mid (x,0)\in\Omega\}$
        \end{itemize}

    \end{whitebox}

    \blue{Algorithm}
    \begin{whitebox}{}
        \footnotesize 
        \textbf{-- Offline --}
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Design stabilizing feedback $\kappa$ with RPI set $\Omega$ \\
            (\emph{e.g.} using incremental stability, linearization and LMIs)
            \item Compute tightened constraints $\bar{\mathcal{Z}}$
            \item Design terminal cost $l_f$ and terminal set $\X_f$ for nominal system
        \end{enumerate}
        \textbf{-- Online --}
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Measure / estimate state $x$
            \item Solve tube-MPC problem
            \item Apply $u=\kappa(x,z_0^\star(x), v_0^\star(x))$
        \end{enumerate}
    \end{whitebox}

    \textbf{TUBE-MPC -- COMPUTE CONSTRAINT TIGHTENING? MAYBE ADD?}

    \blue{Summary}
    \begin{highlightbox}{}
        \footnotesize
        \textbf{Idea}
        \begin{itemize}[leftmargin=1em]
            \item Compensate for noise in nominal prediction using stabilizing FB $\kappa$
            \item Methods used: incremental stability \& tube-formulation
        \end{itemize}
        \textbf{Benefits}
        \begin{itemize}[leftmargin=1em]
            \item Specify robustness using disturbance bound $\W$
            \item Guaranteed stability and safety properties 
            \item Feasible set invariant -- we know exactly when controller will work
        \end{itemize}
        \textbf{Cons}
        \begin{itemize}[leftmargin=1em]
            \item Offline design challenging -- computing $\Omega, \kappa$ for general NL systems can be difficult 
            \item Often conservative 
        \end{itemize}
    \end{highlightbox}
    
\end{whitebox}

\begin{whitebox}{\textbf{STATE \& INPUT DEPENDENT DISTURBANCES}}

    \blue{Concept}
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Problem] Simple disturbance bound $w(k)\in\W$ conservative
            \item[Idea] Impose constraint on nominal trajectory s.t. $\|w(x)\|_{\bar{M}} \leq \bar{w}$ \emph{i.e.} avoid areas with too large model uncertainty
        \end{description}
    \end{whitebox}
    

    \blue{MPC Formulation}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            \min_{V,z_0} \ &\textstyle\sum_{i=0}^{N-1} l(z_i, v_i) + l_f(z_n) \\
            \mathrm{s.t.} \ &z_{i+1} = f(z_i,v_i), \quad i=0,\dots,N-1 \\
            &g_j(z_i, v_i) + c_j {\color{color2} \delta} \leq 0, \quad i=0,\dots,N-1, \quad j\in[1,p] \\
            &{\color{color2} \tilde{w}(z_i,v_i) + L_w \delta \leq \bar{w}} \quad i=0,\dots,N-1 \\
            &z_N \in \X_f \\
            &V_\delta(x,z_0) \leq {\color{color2} \delta=\textstyle\frac{\bar{w}}{1-\rho}}
        \end{align*}
    \end{highlightbox}

    \blue{Algorithm}
    \begin{whitebox}{}
        \footnotesize 
        \textbf{-- Offline --}
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Design stabilizing feedback $\kappa$ \& incremental Lyap function $V_\delta$
            \item {\color{color2} Obtain state \& input dependent disturbance bound $\tilde{w}(x,u)$ and set maximal bound $\bar{w}>0$}
            \item Compute constants $\delta, c_j, L_w > 0$
            \item Design terminal cost $l_f$ and terminal set $\X_f$ for nominal system with tightened constraints
        \end{enumerate}
        \textbf{-- Online --}
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Measure/estimate state $x$
            \item Solve tube-MPC problem
            \item Apply $u=\kappa(x,z_0^\star(x),v_0^\star(x))$
        \end{enumerate}
    \end{whitebox}

    \blue{Theoretical Analysis}
    \begin{whitebox}{}
        \footnotesize 
        \textbf{Assumptions}
        \begin{itemize}[leftmargin=1em]
            \item feasible at $k=0$ with $x(0)$
        \end{itemize}
        \textbf{Properties}
        \begin{itemize}[leftmargin=1em]
            \item Recursively feasible, constraints satisfied $\forall k\geq 0$
            for CL system $x(k+1) = f(x(k), \mu_{\mathrm{tube}}(x(k)) + w(k))$
            \item CL converges to RPI set $\{x \mid V_\delta(x,0)\leq \delta\}$
        \end{itemize}
    \end{whitebox}

    \blue{Discussion}
    \begin{whitebox}{}
        \footnotesize 
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Incorporate $w(x,u)$ bound in the design 
            \item[\textbf{+}] Avoid areas with large disturbances $\tilde{w}(x,u)\leq\bar{w}$
            \item[\textbf{+}] Simple Design, tuning variable $\bar{w}$
            \item[\textbf{+}] Same theoretical properties as \emph{standard} tube-MPC
        \end{itemize}
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{--}] Maximal disturbance bound $\bar{w}$ needs to be chosen offline \\
            $\leadsto$ can be unnecessarily cautious
        \end{itemize}
    \end{whitebox}

\end{whitebox}

\begin{whitebox}{\textbf{HOMOTHETIC TUBES}}

    \blue{Concept}
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Problem] Tube-MPC uses constant tube $(x,z)\in\Omega$ but disturbance bound $\tilde{w}(x,u)$ changes along prediction horizon
            \item[Idea] Use flexible ``homothetic'' tube formulation
        \end{description}
    \end{whitebox}
    

    \blue{MPC Formulation}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            \min_{V,z_0} \ &\textstyle\sum_{i=0}^{N-1} l(z_i, v_i) + l_f(z_n) \\
            \mathrm{s.t.} \ &z_{i+1} = f(z_i,v_i), \quad i=0,\dots,N-1 \\
            &{\color{color2} \delta_{i+1} = \tilde{\rho}\delta_i + \tilde{w}(z_i, v_i)} \quad i=0,\dots,N-1 \\
            &g_j(z_i, v_i) + c_j {\color{color2} \delta_i} \leq 0 \quad i=0,\dots,N-1, \quad j\in[1,p] \\
            &(z_N, {\color{color2}\delta_N})\in \X_f \times [0,\bar{\delta}]\\
            &{\color{color2}\delta_0} = V_\delta(x,z_0)
        \end{align*}
    \end{highlightbox}

    \blue{Theoretical Analysis} \textbf{Same as other tube-MPCs}
    \begin{whitebox}{}
        \footnotesize 
        \textbf{Assumptions}
        \begin{itemize}[leftmargin=1em]
            \item Have i-ISS Lyap. function $V_\delta$
            \item Have Lipschitz continuous disturbance bound $\tilde{w}$
            \item Terminal set satisfies $\forall z\in\X_f, \delta\in [0,\bar{\delta}]$
            \begin{itemize}
                \item Positive invariance: $f(z,\pi_f(z))\in \X_f$, $\tilde{\rho}\delta + \tilde{w}(z,\pi_f(z))\leq\bar{\delta}$
                (tube scaling $\bar{\delta}$ in terminal set)
                \item Constraint satisfaction 
                \item $l_f$ local lyapunov function
            \end{itemize}
            \item Feasible at $k=0$ for $x(0)$
        \end{itemize}
        \textbf{Properties}
        \begin{itemize}[leftmargin=1em]
            \item Recursively feasible, constraints satisfied $\forall k\geq 0$
            for CL system $x(k+1) = f(x(k), \mu_{\mathrm{tube}}(x(k)) + w(k))$
            \item CL converges to RPI set $\{x \mid V_\delta(x,0)\leq \delta\}$
        \end{itemize}
    \end{whitebox}

    \blue{Discussion}
    \begin{whitebox}{}
        \footnotesize 
        \textbf{Benefits}
        \begin{itemize}[leftmargin=1em]
            \item Directly use $w(x,u)$ bound in MPC design 
            \item More flexible, less conservative, larger feasible state set
            \item Same theoretical properties as \emph{standard} tube-MPC
            \item Implicitly avoid areas with large disturbances 
        \end{itemize}
        \textbf{Limitations}
        \begin{itemize}[leftmargin=1em]
            \item Slight complexity increase: requires evaluation of $V_\delta(x,z), \tilde{w}(z,v)$ online
        \end{itemize}
    \end{whitebox}
\end{whitebox}