\section{SAFETY FILTER}

\begin{whitebox}{\textbf{MOTIVATION}}
    \blue{Main Idea}
    \begin{whitebox}{}
        \scriptsize
        \textbf{Augment any learning-based controller with safety guarantees}
        
        \underline{\textbf{Goal:}} Verify contrtol input and modifty if required to ensure safety at all future times
    \end{whitebox}

    \blue{Safe Set}
    \begin{whitebox}{}
        \scriptsize
        Set $\mathcal{S}$ is \underline{\textbf{safe set}} for dynamical system wrt constraints $\X$ if $\forall x(0)\in\mathcal{S}$ we have
        \begin{align*}
            x(k)\in\X \quad \forall k\geq 0
        \end{align*}
        \underline{\textbf{Note:}} $\mathcal{S} \neq \X$ and finding maximal $\mathcal{S}$ can be very hard
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{INVARIANCE-BASED SAFETY FILTER}}
    \blue{Safe Set Based on Control Invariance}
    \begin{whitebox}{}
        \scriptsize
        Maximum control invariant set provides largest safe set $\leadsto$ \underline{\textbf{But}} difficult to compute

        \textbf{Control Invariant Set:}

        Set $\mathcal{C}\subseteq \X$ is control invariant if
        \begin{align*}
            x(k)\in\mathcal{C} \qquad \Rightarrow \exists u(k)\in\U \text{ s.t. } f(x(k),u(k)) \in\mathcal{C} \qquad \forall k \in \mathbb{N}^+
        \end{align*}
        Defines states for which exists a controller that will satisfy constraints for all time
    \end{whitebox}

    \blue{Safe Set Based on Invariance}
    \begin{whitebox}{}
        \scriptsize
        Set $\mathcal{O}$ positive invariant set for system $x(k+1) = f(x(k), \pi(x(k)))$ if 
        \begin{align*}
            x(k) \in \mathcal{O} \quad \rightarrow \quad x(k+1) \in \mathcal{O}, \quad \forall k\in\{0,1,\dots\}
        \end{align*}
        if invariant set within constraints, it provides set of initial states from which trajectory never violates system constraints $\leadsto$ $\mathcal{O}\subseteq \X$ is \underline{\textbf{safe set}}
    \end{whitebox}

    \blue{Safety Controller}
    \begin{highlightbox}{}
        \scriptsize 
        Design \underline{\textbf{safety controller}} $\pi_s(x)$ 
        with large corresponding invar set $\mathcal{O}\subseteq\X$ 
        for controlled system $f(x,\pi_s(x))$ while ensuring $\pi_s(x)\in\U \forall x\in\mathcal{O}$
        \begin{align*}
            \pi_f(u_L,x) = 
            \begin{cases}
                u_l & \text{if $f(x,u_l)\in\mathcal{O}$ and $u_L\in\U$} \\
                \pi_s(x) & \text{otherwise (maintains invar properties of $\pi_s$)}
            \end{cases}
        \end{align*}
        \underline{\textbf{Note:}} 1-step-ahead prediction is enough due to invariant set property
    \end{highlightbox}

    \begin{whitebox}{}
        \scriptsize 
        Formulate safe set as level set of \underline{\textbf{safety value function }}
        \begin{align*}
            \mathcal{S} = \{x \in \R^n \mid V(x) \leq 1\}
        \end{align*}

        \underline{\textbf{Invariant set from Lyapunov Function:}} let $V$ be s.t. $V(f(x))-V(x) \leq 0$, then 
        \begin{align*}
            Y := \{x \mid V(x) \leq \alpha\}
        \end{align*}
        a level set of $V$ is an invariant set for all $\alpha \geq 0$
        \begin{align*}
            \leadsto \pi_f(u_L,x) = 
            \begin{cases}
                u_l(x) & \text{if $V(f(x,u_L(x)))\leq 1, u_L(x)\in\U$} \\
                \pi_s(x) & \text{otherwise}
            \end{cases}
        \end{align*}
    \end{whitebox}

    \blue{Computation via SDP for Linear Systems}
    \begin{whitebox}{}
        \scriptsize 
        \underline{\textbf{Approach:}} select control law $\pi_s(x) = Kx$, define safe set as  level set of value function $\mathcal{S}=\{x\mid x^\top P x \leq 1\}$
        (=invariant set)

        \underline{\textbf{Goal:}} maximize $\mathcal{S}$ subj. to invariance-based safe set conditions:
        \begin{itemize}[leftmargin=1.5em]
            \item Invariance $x^\top(A+BK)^\top P (A+BK)x \leq x^\top P x$
            \item Constraint satisfaction $\mathcal{S}\in\X$
            \item Feasibility of safety controller $\pi_s(x) = Kx \in\U \forall x \in \mathcal{S}$
        \end{itemize}

        \underline{\textbf{LMI Formulation Invariance:}} (using Schur's complement)
        \begin{align*}
            \begin{bmatrix}
                P^{-1} & P^{-1}(A+BK)^\top \\
                (A+BK)P^{-1} & P^{-1}
            \end{bmatrix}
            \succeq 0
        \end{align*}

        \underline{\textbf{LMI Formulation Constraints:}} with $Y=KP^{-1}$
        \begin{align*}
            \begin{bmatrix}
                [b_u]_j^2 & [H_u]_j Y \\
                Y^\top[H_u]_j^\top & P^{-1}
            \end{bmatrix}
            \geq 0 \quad \forall j
        \end{align*}
    \end{whitebox}

    \blue{Safe Set Based on \underline{Robust} Invariance}
    \begin{whitebox}{}
        \scriptsize 
        \underline{\textbf{Approach:}} select control law $\pi_s(x) = Kx$, define safe set as  level set of value function $\mathcal{S}=\{x\mid x^\top P x \leq 1\}$
        (=invariant set)

        \underline{\textbf{Goal:}} maximize $\mathcal{S}$ subj. to invariance-based safe set conditions:
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item {\color{color2} Robust} invariance $x^\top P x \leq 1 \Rightarrow x^\top(A+BK)^\top P (A+BK)x \leq 1 \quad {\color{color2} \forall w\in\W}$
            \item Constraint satisfaction $\mathcal{S}\in\X$
            \item Feasibility of safety controller $\pi_s(x) = Kx \in\U \forall x \in \mathcal{S}$
        \end{enumerate}

        \begin{itemize}[leftmargin=1.5em]
            \item Simple approach: compute stabilizing $K$ and RPI set
            \item Optimiz over $K$ \& $P$
            \begin{itemize}[leftmargin=1.5em]
                \item (2), (3) can be written as LMIs in $P^{-1}$
                \item (1) can be bilinear matrix inequality in $P^{-1}, Y=KP^{-1}$ using \underline{\textbf{S-Procedure}}
            \end{itemize}
        \end{itemize}

    \end{whitebox}

    \blue{Effects on Performance}
    \begin{whitebox}{}
        \scriptsize
        \textbf{Filter interventions can negatively affect performance of learning algorithm}

        \underline{\textbf{Common approach:}} inclsion of safety metric in learnin controller
        
        Example: add \underline{\textbf{barrier term}} to cost used in learning algorithm 
        \begin{align*}
            C_s(x,u) = C(x,u) - \gamma \log(1-V(x))
        \end{align*}
        $\leadsto$ barrier to infinity at boundary of safe set
    \end{whitebox}

    \blue{Properties}
    \begin{whitebox}{}
        \scriptsize 
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Works
            \item[\textbf{--}] \textbf{Explicit computation of safe set and controller can result in:}
            \begin{itemize}[leftmargin=1.5em]
                \item conservatism 
                \item challenging design / computation
                \item scalability issues 
            \end{itemize}
        \end{itemize}

        \underline{\textbf{Idea:}} approximate maximum control invariant set with MPC
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{MODEL PREDICTIVE SAFETY FILTER}}
    \blue{Idea}
    \begin{whitebox}{}
        \scriptsize
        Verify safety by planning safe forward trajectory 
        \begin{itemize}[leftmargin=1.5em]
            \item existence of safety trajectory at time $k$ ensures safety at time $k+1$:
            could follow safety trajectory if no new safety trajectory can be found 
        \end{itemize}
    \end{whitebox}

    \blue{Combined Verification \& Safety Controller}
    \begin{whitebox}{}
        \footnotesize 

        \begin{minipage}[c]{0.45\linewidth}
            \vspace{-1em}
            \begin{align*}
                \min \ &\|u_0 - u_L \| \\
                \mathrm{s.t.} \ &x_{i+1} = f(x_i,u_i) \\
                &x_i \in \X, \quad u_i\in\U \\
                &x_N\in\X_f, \quad x_0 = x(k)
            \end{align*}
        \end{minipage}
        \begin{minipage}[c]{0.53\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {10},
                trim = {52mm, 40mm, 7mm, 12mm},
                clip
                ]{14a_safetyFilter2-1.pdf}
            }
        \end{minipage}

        \textbf{Predictive Safety Filter:}
        \begin{itemize}[leftmargin=1.5em]
            \item If learning input \underline{\textbf{safe:}} $u_0^\star = u_L(k)$
            \item If learning input \underline{\textbf{not safe:}} $u_0^\star(k)=$ 'closest' safe input
        \end{itemize}
    \end{whitebox}

    \blue{Discussion}
    \begin{whitebox}{}
        \scriptsize
        \textbf{Invariance-based Safety Filter}
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Scales to high-dimensional systems
            \item[\textbf{+}] Cheap online verification of safety and computation of safety controller
            \item[\textbf{--}] Conservative safe set
        \end{itemize}
        \textbf{Predictive Safety Filter}
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Scales to high-dimensional systems
            \item[\textbf{+}] Reduced conservatism wrt maximum control invariant set
            \item[\textbf{+}] (Conceptually) easily extended to nonlinear systems
            \item[\textbf{--}] Requires solution of an optimization problem
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{ROBUST SAFETY FILTER}}
    
    \blue{Linear Tube-Based Safety Filter}
    \begin{whitebox}{}
        \footnotesize

        \begin{minipage}[c]{0.58\linewidth}
            \vspace{-1em}
            \begin{align*}
                \min \ &\|u_{0|k} - u_L \| \\
                \mathrm{s.t.} \ &z_{i+1|k} = Az_{i|k} + Bv_{i|k} \\
                &z_{i|k} \in {\color{color2}\X \ominus \Omega}, \quad v_{i|k}\in {\color{color2}\U\ominus K\Omega} \\
                &{\color{color2} u_{0|k} = v_{0|k} + K(x(k) - z_{0|k})} \\
                &z_{N|k}\in\X_f, \quad x(k) = {\color{color2} z_{0|k} \oplus \Omega}
            \end{align*}
        \end{minipage}
        \begin{minipage}[c]{0.40\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {25},
                trim = {100mm, 5mm, 5mm, 40mm},
                clip
                ]{14a_safetyFilter2-1.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \blue{Linear Constraint-Tightening Safety Filter}
    \begin{whitebox}{}
        \footnotesize

        \begin{minipage}[c]{0.63\linewidth}
            \vspace{-1em}
            \begin{align*}
                \min \ &\|v_{0|k} - u_L \| \\
                \mathrm{s.t.} \ &z_{i+1|k} = Az_{i|k} + Bv_{i|k} \\
                &z_{i|k} \in {\color{color2}\X \ominus \F_i}, \quad v_{i|k}\in {\color{color2}\U\ominus K\F_i} \\
                &z_{N|k}\in{\color{color2} \X_f\ominus\F_i}, \quad z_{0|k} = x(k)
            \end{align*}
        \end{minipage}
        \begin{minipage}[c]{0.35\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {26},
                trim = {90mm, 5mm, 5mm, 25mm},
                clip
                ]{14a_safetyFilter2-1.pdf}
            }
        \end{minipage}
    \end{whitebox}
\end{whitebox}