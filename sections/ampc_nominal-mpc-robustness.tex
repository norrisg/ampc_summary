\section{NOMINAL MPC ROBUSTNESS}

\begin{whitebox}{\textbf{PROBLEM SETUP}}
    \blue{System Definition} 

    \begin{minipage}[c]{0.48\linewidth}
        \footnotesize
        \begin{whitebox}{}
            \textbf{DT TI Nonlinear System}
            \begin{align*}
                x(k+1) = f(x(k), u(k), w(k))
            \end{align*}
            \textbf{Solution} at timestep $k$
            \begin{align*}
                \phi(k, x(0), U, W)
            \end{align*}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \footnotesize
        \begin{whitebox}{}
            \textbf{Nominal Model} (without noise)
            \begin{align*}
                \bar{x}(k+1) = \bar{f}(\bar{x}(k), u(k))
            \end{align*}
            \textbf{Solution} at timestep $k$
            \begin{align*}
                \bar{\phi}(k, x(0), U)
            \end{align*}
        \end{whitebox}
    \end{minipage}

    \vspace{0.25em}

    \blue{Design Goals} design control law $u(k) = \pi(x(k))$ s.t 
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Satisfies constraints: $x(k) \in \X, u(k) \in \U$ for all disturbance realizations
            \item Stable: converges to neighborhood of origin
            \item Optimizes (expected / worst case) ``performance''
            \item Maximizes the set $\{x(0) \mid \mathrm{(1)-(3)} \ \textrm{met} \}$
        \end{enumerate}
    \end{whitebox}
\end{whitebox}



\begin{whitebox}{\textbf{LYAPUNOV STABILITY -- SETUP}}
    \blue{$\K$-Function} 
    \begin{whitebox}{}
        \footnotesize
        $\gamma: \R_{\geq 0} \to \R_{\geq 0}$ of class $\K$ if 
        continuous, strictly increasing, $\gamma(0)=0$
    \end{whitebox}
    
    
    \blue{$\K_\infty$-Function} 
    \begin{whitebox}{}
        \footnotesize
        $\gamma: \R_{\geq 0} \to \R_{\geq 0}$ of class $\K_\infty$ if 
        $\K$-function \& $\gamma(s)\xrightarrow{s\to\infty}\infty$
    \end{whitebox}

    \blue{$\KL$-Function} 
    \begin{whitebox}{}
        \footnotesize
        $\beta: \R_{\geq 0} \times \mathbb{Z}_{\geq 0} \to \R_{\geq 0}$ of class $\KL$
        if 

        \begin{itemize}[leftmargin=1em]
            \item $\forall$ fixed $k \geq 0$, $\beta(\cdot, k)$ of class $\K$ 
            \item $\forall$ fixed $s \geq 0$, $\beta(s, \cdot)$ non-increasing and $\beta(s,k)\xrightarrow{k\to\infty}0$
        \end{itemize}
    \end{whitebox}
    
    \blue{Positive Definite Function} $V: \R^a \to \R_{\geq 0}$ pos def in $V(0)=0$ and $\exists$ $\K$-function $\alpha$ s.t $V(x) \geq \alpha(\|x\|)$

    \blue{Uniform Continuity} $f(x,y): A \times B \to \R^c$, $A \subseteq \mathbb{a}$, $B \subseteq \mathbb{b}$ 
    uniformly continuous in $x$ $\forall x\in A, y \in B$ iff $\exists$ $K_\infty$-function $\sigma$ s.t
    \begin{align*}
        \| f(x_1,y) - f(x_2,y) \| \leq \sigma(\|x_1 - x_2\|) \forall x_1, x_2 \in A, \forall y \in B
    \end{align*} 

    $\leadsto$ can bound difference by $\K_\infty$-function

    \blue{Robust Positive Invariant Set}
    \begin{highlightbox}{}
        $\mathcal{O}^{\W}$ robust pos. invar. set for $x(k+1) = f(x(k), w(k))$ if 
        \begin{align*}
            x \in \mathcal{O}^{\W} \Rightarrow f(x,w) \in \mathcal{O}^{\W} \forall w \in \W
        \end{align*}
    \end{highlightbox}

    \tcbsubtitle{\textbf{LYAPUNOV STABILITY}}

    \blue{Asymptotic Stability} $\Omega$ pos invar set for $x(k+1) = \bar{f}_\pi(x(k))$
    \begin{highlightbox}{}
        Origin \textbf{A-Stable} in $\Omega$ if $\exists$ $\KL$-function $\beta$ s.t $\forall x(0)\in\Omega$
        \begin{align*}
            \|\bar{\phi}_\pi(j,x(0))\| \leq \beta(\|x(0)\|,j) \forall j\geq 0
        \end{align*}
    \end{highlightbox}

    \blue{Lyapunov Function} $\Omega$ pos invar set for $x(k+1) = \bar{f}_\pi(x(k))$
    \begin{highlightbox}{}
        $V: \R^n \to \R_{\geq 0}$ \textbf{Lyap Function} in $\Omega$ for $x(k+1) = \bar{f}_\pi(x(k))$ if $\exists$ 
        3 $\K_\infty$-functions $\alpha_{\{1,2,3\}}$ s.t \forall x \in \Omega 
        \begin{align*}
            \alpha_1(\|x\|) \leq V(x) &\leq \alpha_2(\|x\|) \\
            V(\bar{f}_\pi(x)) - V(x) &\leq -\alpha_3(\|x\|)
        \end{align*}
    \end{highlightbox}

    \blue{Lyapunov Stability Theorem} $\Omega$ pos invar set for $x(k+1) = \bar{f}_\pi(x(k))$
    \begin{highlightbox}{} 
        \footnotesize
        If $\exists$ Lyap func in $\Omega$ for system, then origin asympt. stable in $\Omega =$ region of attraction
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{MPC FEASIBILITY \& STABILITY}}
    \blue{Main Idea} \textbf{terminal cost \& constraints ensure feas and stab by design}
    \begin{whitebox}{}
        \begin{align*}
            V_N^\star(x(k)) &= \min_U \ l_f(x_N) + \textstyle\sum_{i=0}^{N-1} l(x_i, u_i) \\
            \mathrm{s.t.} \ x_{i+1} &= \bar{f}(x_i, u_i), \quad x_i \in \X\\
            & x_N \in \X_f, \quad x_0 = x(k)
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item $l_f(\cdot)$ \& $\X_f$ chosen to {\color{color2} mimic infinite horizon}
            \item $\X_N$ denotes feasible set
        \end{itemize}
    \end{whitebox}
    

    \blue{Assumptions}
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Stage cost pos def $\leadsto \exists \ \K_\infty$-function $\alpha_l$ s.t \\
            $ \qquad l(x,u) \geq \alpha_l(\|x\|) \forall x \in \X,u \in \U $
            \item Terminal set pos invar under local control law $\pi_f(x)$ \\
            All state \& input constraints satisfied in $\X_f$, origin contained in $\X_f$
            \item Terminal cost is Lyapunov function in $\X_f$ \& satisfies \\
            $ \qquad l_f(x_{i+1}) - l_f(x_i) \leq -l(x_i, \pi_f(x_i)) \forall x_i \in \X_f $ \\
            and $\exists \ \K_\infty$-functions $\alpha_{1,f}, \alpha_{1,f}$ s.t. \\
            $ \qquad \alpha_{1,f}(\|x\|) \leq l_f(x) \leq \alpha_{2,f}(\|x\|) \forall x\in\X_f $
        \end{enumerate}
    \end{whitebox}

    \blue{Theorem} under those 3 assumptions
    \begin{highlightbox}{}
        \footnotesize
        Feasible set $\X_N$ pos invar \& origin asympt. stable for CL system 
        under MPC control law $\pi_N^{\mathrm{nom}}(x)$ with region of attraction $\X_N$
    \end{highlightbox}
    \blue{Proof}
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item \textbf{Recursive Feasibility}
            \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\alph*)}]
                \item Assume feasibility of $x(k)$ with optimal control sequence $\{u_0^\star, \dots, u_{N-1}^\star\}$
                \item At $x(k+1)$ control sequence $\tilde{U} = \{u_1^\star, \dots, u_{N-1}^\star, \pi_f(x_N^\star)\}$ is feasible
            \end{enumerate}
            \textbf{Terminal constraint provides recursive feasibility, $\X_N$ invariant}
            \item \textbf{Asymptotic Stability} $\leadsto$ prove that MPC cost $V_N^\star(x)$ is lyap func
            \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\alph*)}]
                \item Descent property -- \textbf{WRITE ME}
                \item Lower bound -- \textbf{WRITE ME}
                \item Upper bound -- \textbf{WRITE ME}
            \end{enumerate}
        \end{enumerate}
    \end{whitebox}
    
\end{whitebox}

\begin{whitebox}{\textbf{ISS -- INPUT-TO-STATE STABILITY}}

    \resizebox{\linewidth}{!}{
        \includegraphics[
        page = {40},
        trim = {2mm, 30mm, 15mm, 10mm},
        clip
        ]{02_nominalMPC.pdf}
    }

    \blue{System} $x(k+1) = f_\pi(x(k), w(k)), \quad w \in \W$

    \blue{Assumption} for each of the following statements
    \begin{highlightbox}{}
        \footnotesize
        Suppose $\Gamma \subseteq \R^n$ a RPI set for system, with origin is an interior point.
    \end{highlightbox}

    \blue{Input-To-State Stability in $\Gamma$}
    \begin{highlightbox}{}
        \footnotesize 
        System ISS in $\Gamma$ w.r.t. $w$ if there exists a 
        $\KL$ function $\beta$, $\K$ function $\gamma_2$ s.t
        \begin{align*}
            \underbrace{\| \phi_\pi(k,x(0), W)\| \leq \beta(\|x(0)\|,k)}_{\text{asymptotic stability}} 
            + \underbrace{\gamma_2(\| W_{[k-1]} \|)}_{\text{increase as func of seq}}
        \end{align*}
        $\forall x(0) \in \Gamma, W\in\W^{k-1}, k\geq 0$ with $\| W_{[k-1]} \| := \max_{0\leq j\leq k-1}\{\|w(j)\|\}$
    \end{highlightbox}

    \blue{ISS-Lyapunov Function in $\Gamma$}
    \begin{highlightbox}{}
        \footnotesize
        $V:\R^n \to \R_{\geq 0}$ is ISS-Lyap function 
        in $\Gamma$ for system wrt $w$ if there exists suitable $\K_\infty$ 
        functions $\alpha_{\{1,2,3\}}$, $\K$ function $\lambda_2$ s.t
        $\forall x\in\Gamma, w\in\W$
        \begin{align*}
            \alpha_1(\|x\|) \leq &V(x) \leq \alpha_2(\|x\|) \\
            V(f_\pi(x,w)) - &V(x) 
            \leq \underbrace{-\alpha_3(\|x\|)}_{\text{no increase }f(x)} 
            + \underbrace{\lambda(\|w\|)}_{\text{allow increase }f(w)}
        \end{align*}
    \end{highlightbox}

    \blue{Theorem}
    \begin{highlightbox}{}
        \footnotesize
        If System admits ISS-Lyap function in $\Gamma$ wrt $w$ then it is ISS in $\Gamma$ wrt $w$
    \end{highlightbox}

    \blue{Remarks}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item ISS stability holds for any given disurbance set. \textbf{BUT:}
            \begin{itemize}[leftmargin=1em]
                \item ROA (RPI set $\Gamma$) depends on disturbance set 
                \item Trajectories of system converge asympt. to set which depends on disturbance set 
            \end{itemize}
            \item \textbf{Inherent robustness of nominal MPC corresponds to existence of small enough disturbance bound s.t ISS (or similar) holds}
            \item There are direct connections between ISS and other robust stability notions \emph{i.e} asymptotic gain, robust stability margin
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{ISS OF NOMINAL MPC}}
    \blue{Nominal MPC Controller} $\leadsto$ ignore noise
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            V_N^\star(x(k)) = \min_U \ &l_f(x_N) + \textstyle\sum_{i=0}^{N-1} l(x_i,u_i) \\
            \mathrm{s.t.} \ &x_{i+1} = \bar{f}(x_i,u_i),  \quad x_i \in \X \\
            &x_N \in \X_f, \quad x_0 = x(k)
        \end{align*}
        
    \end{whitebox}

    \blue{System} $x(k+1) = f(x(k),u(k),w(k)) \leadsto$ with noise

    \blue{Main Result}
    \begin{highlightbox}{}
        \footnotesize
        Let $f(x, \pi_N^{\textrm{nom}},w)$ be \textbf{uniformly continuous} in $w \forall x \in \X_N, w \in \W$. \\
        Suppose \textbf{ONE} of the following holds
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Function $\bar{f}(x, \pi_N^{\textrm{nom}})$ is uniformly continuous in $x \forall x \in\X_N$
            \item Optimal cost $V_N^\star(x)$ uniformly continuous in $\X_N$
        \end{enumerate}
        Then $\exists$ RPI set $\Omega_r\subseteq \X_N$ for sufficiently small bound on $w$, s.t. system controlled by nominal MPC controller $u(k)=\pi_N^{\textrm{nom}}(x)$ is ISS in $\Omega_r$ 
    \end{highlightbox}

    \blue{Cases When Nominal MPC Leads to ISS}
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=3em, label=\textbf{C\arabic* --}]
            \item Nominal model linear, cost $V_N(x,U)$ linear or quad, constraints polytopes
            then \textbf{optimal cost $V_N^\star(x)$ uniformly continuous in $\X_N$}
            \item If plant only constrained on inputs, terminal region $\X_f = \R^n = \mathbb{X}_N$
            and $V_N(x,U)$ uniformly continuous in $x \forall x \in\R^n$ and $U\in\U^{N-1}$ 
            then \textbf{optimal cost $V_N^\star(x)$ uniformly continuous in $\R^n$}

            \textbf{Note --} if $f(x,u,w)$ \& $l(x,u)$ uniformly continuous in $x$, 
            and $V_f(x)$ uniformly continuous $\forall x \in \R^n, u\in\U, w\in\W$,
            then uniform continuity of $V_N(x,U)$ holds
            \item If system has constraints on states and inputs, $l_f(x) := \lambda V(x)$, 
            with $V(x)$ uniform continuous Lyap function in $\X_f$, $\lambda \geq 1$, 
            $V_N{x,U}$ uniformly continuous in $x \forall x\in\X_N$ and $U\in \U^{N-1}$

            Then there exists region $\Omega_r = \{x\in\R^n \mid V_N^\star(x) \leq r\} \subset \X_N$

            \textbf{Intuition -- Formulation must be s.t. state constraints not active in RPI set where system is then ISS}
        \end{enumerate}
    \end{whitebox}
    \blue{Remarks}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item If instead of uniform cont in previous results, only local cont in neighborhood of $x=w=0$, results can be adapted to show local ISS
            \item Extensions of subopt MPC sol'ns exist, if optimal MPC cost cont
            \item Various generalizations under reduced assumptions exist (e.g. avoiding certain continuity assumptions)
        \end{itemize}
    \end{whitebox}

    \tcbsubtitle{\textbf{SOFT-CONSTRAINTED NOMINAL MPC}}

    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            V_N^{\mathrm{soft}}(x(k)) = \min_{U,{\color{color2}\epsilon_i}} \ &l_f(x_N) + {\color{color2}l_\epsilon(\epsilon_N)} + \textstyle\sum_{i=0}^{N-1} l(x_i,u_i) + {\color{color2}l_\epsilon(\epsilon_i)} \\
            \mathrm{s.t.} \ &x_{i+1} = \bar{f}(x_i,u_i), \quad u_i \in \U, \quad x_0 = x(k) \\
            &{\color{color2}H_x x_i \leq c_x + \epsilon_i}, \quad {\color{color2}H_N x_N \leq c_N + \epsilon_N}
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item $l_\epsilon$ penalty on slack variables \emph{e.g.} $v_1 \|\epsilon\|_1 + v_2 \|\epsilon\|_2^2$
            \item $\X_N^{\mathrm{soft}}$ denotes enlarged feasible set 
            \item \textbf{No hard state constraints}
            \item \textbf{ACHTUNG: $V_N^{\mathrm{soft}}(x(k))$ not Lyap function in $\X_N^{\mathrm{soft}}$ without further assumptions}
        \end{itemize}
    \end{whitebox}

    \tcbsubtitle{\textbf{SUMMARY -- NOM. MPC FOR UNCERTAIN SYSTEMS}}
    \blue{Idea} $\leadsto$ ignore noise and hope works

    \blue{Benefits}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Simple, Often effective in practice 
            \item No knowledge of noise set $\W$ $\leadsto$ \emph{just works}
            \item Feasible set larger (can find sol'n, but may not work)
            \item ROA might be larger than other approaches
        \end{itemize}
    \end{whitebox}

    \blue{Cons}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Works for linear systems, for nonlinear systems only under stronger cont assumptions
            (absence of state constraints, specific formulations)
            \item Very difficult to determine ROA 
            \item Hard to tune -- no obvious way to tradeoff robustness vs perf
        \end{itemize}
    \end{whitebox}
\end{whitebox}