\section{LBMPC -- LEARNING-BASED MPC}

\begin{whitebox}{\textbf{OVERVIEW}}
    \scriptsize
    \begin{tabular}{l l l c c}
        \textbf{Method} & \textbf{Type} & \textbf{Regression} & \textbf{Uncertainty Update} & \textbf{Guarantees} \\
        \midrule 
        RP-LBMPC & Robust & Any & X & \checkmark \\
        RA-MPC & Robust & Set-member. & \checkmark & \checkmark \\
        GP-MPC & Stoch. & BLR/GP & [\checkmark] & [Some]
        
    \end{tabular}
\end{whitebox}

\begin{whitebox}{\textbf{RP-LBMPC -- Robust Performance LBMPC}}

    \blue{System}
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            x(k+1) = 
            \underbrace{Ax(k) + Bu(k)}_{\text{nominal dynamics}}
            +
            \underbrace{g(x(k),u(k)) + w(k)}_{:= \bar{w}(k) \text{ model error}}
        \end{align*}
    \end{whitebox}

    \blue{Approach} seperate into 2 models
    \begin{whitebox}{}
        \scriptsize 
        \centering
        \begin{tabular}{l l l}
            Nominal Lin Model & $z_{i+1} = A z_i + Bu_i$ &  constraints $\leadsto$ safety \\
            Learned N-Lin model & $x_{i+1} = Ax_i + Bu_i + \mathcal{O}_k(x_i,u_i)$ & cost $\leadsto$ performance
        \end{tabular}
    \end{whitebox}

    \blue{Robust Performance LBMPC}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{alignat*}{2}
            \min_{u} \ &\textstyle\sum_{i=0}^{N-1} l(x_i, u_i) + l_f(x_N) \\
            \mathrm{s.t.} \ &x_{i+1} = Ax_i + Bu_i + \mathcal{O}_k(x_i, u_i) \qquad && \text{for performance} \\
            &{\color{color2}z_{i+1} = Az_i + Bu_i} && {\color{color2}\text{for safety}} \\
            &{\color{color2}z_i \in \X \ominus \F_i}, \quad u_i \in \U \ominus K\F_i \\
            &{\color{color2}z_N \in \X_f \ominus \F_N} \\
            &\bar{x}_0 = x(k), \quad {\color{color2}z_0 = x(k)}
        \end{alignat*}
    \end{highlightbox}

    \begin{minipage}[t]{0.48\linewidth}
        \blue{Terminal Invariant Set}
        \begin{whitebox}{}
            \scriptsize
            Assuming stable $K$, find RPI set $\X_f$
            \begin{align*}
                &\X_f \subseteq \{x \mid x\in\X, Kx \in \U \} \\
                &(A+BK)\X_f \oplus \W \subseteq \X_f
            \end{align*}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[t]{0.48\linewidth}
        \blue{Constraint Tightening}
        \begin{whitebox}{}
            \scriptsize
            Assuming $\bar{w}(k)\in\W$
            \begin{align*}
                \F_0 &= 0, \quad
                \F_i &= \bigoplus_{j=0}^{i-1}(A+BK)^j\W
            \end{align*}
        \end{whitebox}
    \end{minipage}
    
    \blue{Performance \& Safety}
    \begin{whitebox}{}
        \scriptsize 
        \begin{itemize}[leftmargin=1.5em]
            \item Cost functino depends on states of learned model, which uses $\mathcal{O}_k$
            \item Recursive feasibility / robust constraint satisfaction not depend on $\mathcal{O}$
            \item $\mathcal{O}_k$ can be time-varying as we want to update learned model
            \item \textbf{While performance model can be updated online, uncertainty set $\W$ cannot as cannot ensure $\W_{k+1} \subseteq \W_k$}
        \end{itemize}
    \end{whitebox}

    \blue{Robust Feasibility \& Constraint Satisfaction}
    \begin{whitebox}{}
        \scriptsize 
        If $\X_f$ is robust terminal invar set and $U$ is feasible for RP-LBMPC scheme, then applying $u(k)=u_0^\star$ gives 
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Recursive feasibility
            \item Robust constraint satisfaction $x\in\X, u\in\U$
        \end{enumerate}
    \end{whitebox}

    \blue{Lemma -- Continuity of Value Function}
    \begin{whitebox}{}
        \scriptsize 
        Let $\X_N$ be feasible set of RP-LBMPC. IF functions $l, l_f, \mathcal{O}_k$ continuous, then $J_k^\star(x(k))$ continuous on $\mathrm{int}(\X_N)$
    \end{whitebox}

    \blue{ISS -- Input To State Stability}
    \begin{whitebox}{}
        \scriptsize 
        Assume
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Terminal set $\X_f$ RPI 
            \item LBMPC opt problem feasible for $x(0)$
            \item Stage cost $l$, terminal cost $l_f$ \textbf{strictly convex, continuous, time-invariant}
            \item System controlled with RP-LBMPC and $\mathcal{O}_k(x,u)=0$ is ISS
        \end{enumerate}
        Then system in CL with RP-LBMPC is ISS wrt model mismatch $g(x,u)+w$ and oracle $\mathcal{O}_k(x,u)$
    \end{whitebox}

\end{whitebox}

\begin{whitebox}{\textbf{SET MEMBERSHIP ESTIMATION}}
    \blue{Idea}
    \begin{whitebox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            y = \theta^\top x + w
        \end{align*}
        Assume \underline{\textbf{bound}} on $w$ and successively compute \underline{\textbf{all}} values of $\theta$ that are consistent with observations $y_i$ under this assumption
    \end{whitebox}

    \blue{Non-Falsified Parameter Set}
    \begin{whitebox}{}
        \scriptsize 
        Given measurement $(x_i,y_i)$ the non-falsified paramter set given as 
        \begin{align*}
            \Delta_i &= \{\Theta \mid y_i - \Theta x_i \in \W \} = \{ \Theta \mid -H_w \Theta x_i \leq h_w - H_w y_i \} \\
            &= \{ \Theta \mid \underbrace{-(x_i^\top \otimes H_w)}_{H_{\Delta_i}} \mathrm{vec}(\Theta) \leq \underbrace{h_w - H_w y_i}_{h_{\Delta_i}} \}
        \end{align*}
        which is a polytope -- using $\mathrm{vec}(AXB) = (B^\top \otimes A)\mathrm{vec}(X)$
    \end{whitebox}

    \blue{Set-Membership Estimation Algorithm}
    \begin{highlightbox}{}
        \scriptsize
        \begin{enumerate}[leftmargin=3em]
            \item[0.1] Assume (robust bound on disturabnce value $w\in\W$)
            \item[0.2] Assume prior (robust) bound on parameter value $\theta\in\Omega_0$
            \item[0.3] Set $i=1$
            \item[1. ] Observe $y_i = \theta x_i + w_i$ at location $x_i$
            \item[2. ] Compute set of all $\theta$ that could give rise to $y_i$: \textbf{non-falsified set} $\Delta_i$
            \item[3. ] Compute intersection $\Omega_i=\Omega_{i-1}\cap\Delta_i$ 
            \item[4. ] $i=i+1$, GOTO 1.    
        \end{enumerate}
    \end{highlightbox}

    \blue{Constraint Reduction}
    \begin{whitebox}{}
        \scriptsize
        Each intersection adds $q_w$ constraints. Many of these may be \underline{\textbf{redundant}}
        \begin{itemize}[leftmargin=1.5em]
            \item Can be checked with linear program (LP)
            \item Constraint removal can be expensive since LP must be solved
            \item Exact parameter set may still require infinite number of constraints
        \end{itemize}
    \end{whitebox}

    \blue{Fixed Shape Parameter Set}
    \begin{whitebox}{}
        \scriptsize 
        To overcome computational burden, fix shape of parameter sets $\Omega_i$
        \begin{align*}
            \Omega_i := \{\Theta | H_\theta \mathrm{vec}(\Theta) \leq h_{\theta_i}\}
        \end{align*}
        and only adjust $h_i$ in each step 
        \begin{enumerate}[leftmargin=3em]
            \item Compute non-falsified set $\Delta_i$ as before
            \item Find \emph{minimum} $\Omega_i$ s.t $\Omega_i \supseteq  \Omega_{i-1} \cap \Delta_i$ while ensuring $\Omega_i \subseteq \Omega_{i-1}$
        \end{enumerate}
        Can be formulated as linear program
    \end{whitebox}

    \blue{Point Estimation}
    \begin{whitebox}{}
        \scriptsize
        To use the model for predictionw need point estimate $\hat{\theta}$
        \begin{itemize}[leftmargin=1.5em]
            \item \textbf{Center of Polytope} ensures $\hat{\theta}_i \in \Omega_i$
            \begin{itemize}[leftmargin=1.5em]
                \item Chebyshev center
                \item Analytic center
            \end{itemize}
            \item \textbf{Projection of arbitrary point estimate} onto $\Omega_i$
            \begin{itemize}[leftmargin=1.5em]
                \item Bayesian Linear Regression
                \item Kalman Filters
                \item Least Mean Squares Filter
            \end{itemize}
        \end{itemize}
    \end{whitebox}
    
\end{whitebox}

\begin{whitebox}{\textbf{RA-MPC -- ROBUST ADAPTIVE MPC}}
    
    \blue{RA-MPC -- Robust Adaptive MPC}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{alignat*}{2}
            \min_{v,z,\alpha,\Lambda} \ &\textstyle\sum_{i=0}^{N-1} \|\hat{x}_i\|_Q^2 \|\hat{u}_i\|_R^2 + \|\hat{x}_N\|_P^2 \\
            \mathrm{s.t.} \ &\hat{x}_i = A_{\hat{\theta}_k}\hat{x}_i + B_{\hat{\theta}_k}\underbrace{(K\hat{x}_i + v_i)}_{\hat{u}_i} \\
            &\hat{x}_0 = x(k), \\
            &\text{Linear constraints ensuring robustness wrt $\theta\in\Omega_k$ ($\F_i$)}
        \end{alignat*}
        \begin{tabular}{c l c l}
            $\{v_i\}$ & affine predicted input & $\{\alpha_i\}$ & size of $\F_i$ \\
            $\{z_i\}$ & position of $\F_i$ & $\{\Lambda_i\}$ & to verify nec. prop. of $\F_i$
        \end{tabular}
    \end{highlightbox}

    \blue{Automatic Controller Adaptation Algorithm}
    \begin{whitebox}{}
        \scriptsize
        \begin{enumerate}[leftmargin=3em, start=0]
            \item Initialize polytopic $\Omega_0$, set $k=1$
            \item Measure $x(k)$
            \item Set-Membership update 
            \begin{enumerate}[leftmargin=3em]
                \item Update point estimate $\hat{\theta}_k$
                \item Update SM parameter uncertainty description $\Omega_k \supseteq \Omega_{k-1}\cap\Delta_k$
            \end{enumerate}
            \item Robust MPC 
            \begin{enumerate}[leftmargin=3em]
                \item Formulate MPC $\mathcal{D}(\mathbb{W}_k = (\Omega_k, \W, \hat{\theta}_k)) = \pi_{\mathbb{W}_k}$
                \item Solve MPC optimization to get $v_0^\star$
                \item Apply $u(k) = \pi_{\mathbb{W}_k}(x(k)) = Kx(k) + v_0^\star$
            \end{enumerate}
            \item set $k=k+1$, GOTO 1.
        \end{enumerate}
    \end{whitebox}

    \blue{Summary}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1.5em]
            \item Set-membership estimation as \emph{robust} regression technique
            \item Allows \underline{\textbf{automatic}} uncertainty update in MPC \underline{\textbf{with guarantees}}
            \item Equally applicable to nonlinear systems if parameters enter linearly
            \item Practical implementation feasible, but largely remains to be investigated
        \end{itemize}
    \end{whitebox}

\end{whitebox}

\begin{whitebox}{\textbf{GP-MPC -- GAUSSIAN PROCESS MPC}}
    \blue{True System}
    \begin{whitebox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            x(k+1) = \tilde{f}(x(k),u(k),\theta) + \tilde{w}(k)
        \end{align*}
    \end{whitebox}

    \blue{Joint In Time Chance Constraints}
    \begin{whitebox}{}
        \scriptsize 
        Robustness in probability requires all (usually infinite) future time steps \underline{\textbf{jointly}}
        \begin{align*}
            \mathrm{Pr}\{x(k+1) - f(x(k),u(k),\hat{\theta}) \in \W \quad {\color{color2}\forall k \geq 0} \} \geq p
        \end{align*}
        Chance constraints looked at so far need to hold for all times \underline{\textbf{individually}}
        \begin{align*}
            \mathrm{Pr}\{x(k)\in\X\} \geq p \quad {\color{color2}\forall k \geq 0}
        \end{align*}
    \end{whitebox}

    \blue{Application to Bayesian Linear Regression}
    \begin{whitebox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            x(k+1) = \phi(x(k), u(k))\theta + w(k) \quad \text{i.i.d.} w(k) \sim \mathcal{N}(0,\Sigma^w)
        \end{align*}
        given normal prior on $\theta$ and measurement data $\mathcal{D}=\{x(k+1)\phi(x(k),u(k))\}_{k=0}^{n_d}$

        Posterior paramter distrubution is Gaussian
        \begin{align*}
            p(\theta | \mathcal{D}) = \mathcal{N}(\mu_\theta, \Sigma_\theta) 
        \end{align*}
        and with $\Omega= \{\theta \mid (\theta-\mu_\theta)^\top \Sigma_\theta^{-1}(\theta-\mu_\theta) \leq \xi_n^2(p)\}$ we have 
        \begin{align*}
            \mathrm{Pr}\{\theta\in\Omega \mid \mathcal{D}\} = p
        \end{align*}
    \end{whitebox}

    \blue{Robust In Probability MPC}
    \begin{highlightbox}{}
        \footnotesize 
        Assume \underline{\textbf{true system}} given by 
        \begin{align*}
            x(k+1) = \phi(x(k), u(k))\theta + \tilde{w}(k) 
        \end{align*}
        with parameter set $\Omega$ s.t $\mathrm{Pr}\{\theta\in\Omega\} \geq p$

        Assume/ensure $\tilde{w}(k)\in\W \quad \forall k\geq 0$ $\leadsto$ reduces to \underline{\textbf{robust problem:}}
        \begin{align*}
            x(k+1) = \phi(x(k), u(k))\theta + \tilde{w}(k) \quad \forall \theta\in\Omega, \tilde{w}(k)\in\W
        \end{align*}  

        for which we have seen solutions (last lecture) $\leadsto$ valid with probability $p$
    \end{highlightbox}
    \blue{Problems}
    \begin{whitebox}{}
        \scriptsize
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item $\tilde{w}(k)\in\W \quad \forall k \geq 0$ inconsistent with Gaussian assumption for estimation 
            \item Feasibility after uncertainty update can usually not be ensured
        \end{enumerate}
    \end{whitebox}

    \blue{Computational Approximations for GPs}
    \begin{whitebox}{}
        \scriptsize 
        GP regression non-parametric $\leadsto$ evaulation expensive as collect more data

        Typical ways to address:
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Use sparse spectrum GP approximation 
            \begin{itemize}[leftmargin=1.5em]
                \item Construct finite number of basis functions to approximate GP
            \end{itemize}
            $\leadsto$ can be treated as parametric regression
            \item Make use of reduced number of \underline{\textbf{inducintg inputs}}
            \begin{itemize}[leftmargin=1.5em]
                \item Construct inducint input locations to approximate GP
                \item Computation times scale mainly with \# of inducing inputs
            \end{itemize}
            $\leadsto$ remains fully non-parametric
        \end{enumerate}
    \end{whitebox}

    \blue{Kalman Filtering}
    \begin{whitebox}{}
        \scriptsize
        Dynamics can change $\leadsto$ introduce \underline{\textbf{parameter dynamics}}
        \begin{align*}
            {\color{color2}\theta(k+1)} \ {\color{color2}= \theta(k) + w_\theta(k)} \quad \leadsto \quad x(k+1) = \phi(x(k), u(k))\theta(k) + w(k)
        \end{align*}
        \underline{\textbf{Corresponds exactly to Kalman filter setup}}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{BO -- BAYESIAN OPTIMIZATION}}
    \blue{Problem Setup}
    \begin{whitebox}{}
        \scriptsize
        \textbf{Goal:} minimize unknown function $h$ wrt some parameters $q$
        \begin{align*}
            \min_{q\in\mathcal{Q}\in\R^{n_q}} h(q)
        \end{align*}
        $h(q)$ unknown, might not have analytical form, noisy, expensive (sample at most $T$ times)
        
        \textbf{Question:} how to pick $q_1, \dots, q_T$ in order to find $\min_q h(q)$
    \end{whitebox}

    \blue{Bayesian Optimization Algorithm}
    \begin{whitebox}{}
        \scriptsize
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {28},
            trim = {5mm, 15mm, 5mm, 12mm},
            clip
            ]{12_BO-1.pdf}
        }
        \textbf{Note} surrogate model / GP model can be updated offline / between experiments
    \end{whitebox}

    \blue{Acquisition Funciton}
    \begin{whitebox}{}
        \scriptsize 
        The acquisition function suggests the next input location to sample
        \begin{itemize}[leftmargin=1.5em]
            \item \textbf{Lower/Upper confidence bound}
            \item \textbf{Thompson sampling}
            \item Expected improvement
            \item Probability of improvement
            \item Entropy search 
            \item Random
        \end{itemize}
    \end{whitebox}

    \blue{LCB -- Lower Confidence Bound} {\scriptsize\textbf{Optimism in Face of Uncertainty}}
    \begin{highlightbox}{}
        \footnotesize 
        Pick next sample that minimizes lower confidence bound:
        \begin{align*}
            q_i = \arg\min_{q} \mu_{i-1}^h(q) - \beta \sqrt{\Sigma_{i-1}^h(q)}
        \end{align*}
    \end{highlightbox}

    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {39},
            trim = {50mm, 5mm, 50mm, 35mm},
            clip
            ]{12_BO-1.pdf}
        }
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {40},
            trim = {50mm, 5mm, 50mm, 35mm},
            clip
            ]{12_BO-1.pdf}
        }
    \end{minipage}

    \blue{Thompson Sampling} 
    \begin{highlightbox}{}
        \footnotesize 
        At iteration $i$ draw sample function from GP and compute 
        \begin{align*}
            \tilde{h}(q) \sim \mathcal{GP}(\mu_{i-1}^h, \Sigma_{i-1}^h) \qquad \leadsto q_i = \arg\min_{q} \tilde{h}(q)
        \end{align*}
    \end{highlightbox}

    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {48},
            trim = {50mm, 5mm, 50mm, 35mm},
            clip
            ]{12_BO-1.pdf}
        }
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {49},
            trim = {50mm, 5mm, 50mm, 35mm},
            clip
            ]{12_BO-1.pdf}
        }
    \end{minipage}

\end{whitebox}

\begin{whitebox}{\textbf{MPCC -- MODEL PREDICTIVE CONTOURING CONTROL}}
    \blue{Problem Statement}
    \begin{whitebox}{}
        \scriptsize
        \begin{minipage}[c]{0.58\linewidth}
            Dynamic System with position states $X,Y$

            Progress as additional state $\rho_i$, increment as input $\nu_i$
            \begin{align*}
                \rho_{i+1} = \rho_i + \nu_i
            \end{align*}

            \underline{\textbf{Goal:}} Follow path accurately \& quickly

            \underline{\textbf{Design cost function $l(x,u)$ which:}}
            \begin{itemize}[leftmargin=1.5em]
                \item Encourages progress 
                \item Penalizes error
            \end{itemize}
            and additionally introduce constraints
        \end{minipage}
        \begin{minipage}[c]{0.40\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {59},
                trim = {83mm, 10mm, 8mm, 10mm},
                clip
                ]{12_BO-1.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \blue{Optimization Problem}
    \begin{whitebox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            \min_{u,x} \ &\textstyle\sum_{i=1}^{N-1} q_{\mathrm{lag}}(e_i^l)^2 + q_{\mathrm{cont}}(e_i^c)^2 - q_{\mathrm{adv}}\nu_i \\
            \mathrm{s.t.} \ &x_{i+1} = f(x_i,u_i), \quad x_i\in\X, u_i\in\U, \quad x_0=x(k)
        \end{align*}
        where contouring and lag errors $e^c, e^l$ nonlin-ly depend on state \& reference path 
    \end{whitebox}

    \blue{Tuning Objective}
    \begin{whitebox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            h(q) = T_{\mathrm{lap}}(q) + \lambda \Delta_{\mathrm{centerline}}(q)
        \end{align*}

        \begin{itemize}[leftmargin=1.5em]
            \item $q=[q_{\mathrm{adv}}, q_{\mathrm{cont}}, q_{\mathrm{lag}}]$
            \item $T_{\mathrm{lap}}$ is observed lap-time during flying lap
            \item $\Delta_{\mathrm{centerline}}$ is average distance to centerline 
            \item $\lambda$ is safety-performance trade-off parameter
        \end{itemize}
    \end{whitebox}
\end{whitebox}