\section{SMPC -- STOCHASTIC MODEL PREDICTIVE CONTROL}

\begin{whitebox}{\textbf{STOCHASTIC PROBLEM SETTING}}
    \blue{System}
    \begin{highlightbox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            x(k+1) &= f(x(k), u(k)) + w(k) 
            \quad \mathrm{Pr}\{ x(k)\in\X \} \geq p 
            \quad \mathrm{Pr}\{ u(k)\in\U \} \geq p \\
            w(k) &\sim \mathcal{Q}^w \text{(iid)}
        \end{align*}
    \end{highlightbox}

    \blue{Design Goals}
    \begin{whitebox}{}
        \scriptsize
        Design control law $\pi(x(k))$ s.t. system
        \begin{enumerate}[leftmargin=3em, label=\textbf{(\arabic*)}]
            \item Satisfies constraints with given probability
            \item Is `stable': converges to origin in suitable sense
            \item Optimizes \emph{expected} ``performance''
            \item Maximizes set $\{x(0) | \text{conditions (1)-(3) met}\}$
        \end{enumerate}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{STOCHASTIC STABILITY}}
    \blue{Mean-Square Stability}
    \begin{whitebox}{}
        \footnotesize
        Eq. point $\bar{x}=0$ \textbf{mean-square stable} if $\forall \epsilon > 0$ there exists $\delta(\epsilon)$ s.t
        \begin{align*}
            \|x(0)\| < \delta(\epsilon) \Rightarrow \mathbb{E}(\|x(k)\|) < \epsilon, \quad \forall k \geq 0
        \end{align*}
        if  $\lim_{k\to\infty}\mathbb{E}(\|x(k)\|) =0$ then \textbf{asymptotically} mean-square stable
    \end{whitebox}
    \blue{ACHTUNG} not applicable for persistent additive noise!

    \blue{Asymptotic Average Performance}
    \begin{highlightbox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            l_{\mathrm{avg}} = \lim_{\bar{N}\to\infty} \frac{1}{\bar{N}} \textstyle\sum_{k=0}^{\bar{N}-1} \mathbb{E}\{ l(x(k),\pi(k)) \} \leq C
        \end{align*}
        typically shown using Lyapunov-like decrease 
        \begin{align*}
            \mathbb{E}\{V(x^+)\mid x\} - V(x) \leq -l(x,\pi) \ {\color{color2} + C}
        \end{align*}
        Like ISS: not just decrease but let go to constant
    \end{highlightbox}

    \blue{Stochastic Cost Decrease}
    \begin{whitebox}{}
        \scriptsize 
        Stochastic MPC Lyapunov decrease 
        \begin{align*}
            {\color{color2}\mathbb{E}\{} l_f(f(x_N^\star,\pi_f,w)) {\color{color2}\}}
            - {\color{color2}\mathbb{E}\{} l_f(x_N^\star) {\color{color2}\}}
            + {\color{color2}\mathbb{E}\{} l(x_N^\star, \pi_f) {\color{color2}\}}
            -l(x(k,u_0^\star))
        \end{align*}
        Assumption 
        \begin{align*}
            {\color{color2}\mathbb{E}\{} l_f(f(x_N^\star,\pi_f,w)) {\color{color2}\}}
            - {\color{color2}\mathbb{E}\{} l_f(x_N^\star) {\color{color2}\}}
            \leq {\color{color2}\mathbb{E}\{} l(x_N^\star, \pi_f) {\color{color2}\} + C}
        \end{align*}
        implies 
        \begin{align*}
            {\color{color2}\mathbb{E}\{} J^\star(x^+ | x) {\color{color2}\}}
            - J^\star(x)
            \leq -l(x, u_0^\star) \ {\color{color2}+ C}
        \end{align*}
        $\leadsto$ Decay by stage cost plus constant term
    \end{whitebox}
    \blue{Quadratic Cost Decrease} {\scriptsize \textbf{TO MAKE TRACTABLE}}
    \begin{highlightbox}{}
        \scriptsize 
        Unconstrained linear quad MPC has better (or equal) performance as tube controller K
        when choosing corresponding terminal cost $P=A_K^\top P A_K + Q + K^\top R K$
        \begin{align*}
            \lim_{\bar{N}\to\infty} \frac{1}{\bar{N}} 
            \textstyle\sum_{k=0}^{\bar{N}-1} 
            \mathbb{E}\{ l(x(k),u(k)) \mid x(0) \} 
            \leq {\color{color2}\mathrm{tr}\{P\Sigma_w\}}
        \end{align*}
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{STOCHASTIC CONSTRAINT-TIGHTENING MPC}}

    \blue{Concept}
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Problem] unbounded stochastic $w$ can drive $x$ into infeasible region
            \item[Idea] Assume maximum size of disturbance $\leadsto$ robust techniques
        \end{description}
    \end{whitebox}

    \blue{Recursive Feasibility}
    \begin{highlightbox}{}
        \scriptsize 
        \vspace{-0.5em}
        \begin{align*}
            \underbrace{\text{Robustly ensure \textbf{chance} constraints}}_{\text{SMPC with bounded disturbances}}
            \neq 
            \underbrace{\text{Robustly ensure \textbf{deterministic} constraints}}_{\text{Robust MPC}}
        \end{align*}
    \end{highlightbox}
    
    \blue{Chance Constraints} $\F_w(p)$ set contains $w$ with prob $p$
    \begin{whitebox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            {\color{color2} \mathrm{Pr}(}
            \{ {\color{color2}\bar{x}_i + w_{i-1}} \} 
            \oplus A_K\W \oplus \dots \oplus A_K^{i-1}\W
            \subseteq \X{\color{color2}) \geq p}  \\
            \Leftarrow \qquad 
            {\color{color2}\{\bar{x}_i\} \oplus \F_w(p)} \oplus (A+BK)\F_{i-1} \subseteq \X
        \end{align*}
    \end{whitebox}

    \blue{Constraint Tightening MPC} {\scriptsize\textbf{NEGLECTING INPUT CONSTRAINTS}}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            \min_{\bar{u}_i} \ &\textstyle\sum_{i=0}^{N-1} \|\bar{x}_i\|_Q^2 + \|\bar{u}_i\|_R^2 + \|\bar{x}_N\|_P^2 \\
            \mathrm{s.t.} \ &\bar{x}_{i+1} = A\bar{x}_i + B\bar{u}_i \quad i=0,\dots,N-1 \\
            &\bar{x}_i \in \X 
            \underbrace{\ominus \ (A+BK)\F_{i-1}}_{\text{i-1-steps robust}}
            {\color{color2}\underbrace{\ominus \ \F_w^x(p)}_{\text{1-step stochastic}}}
            \quad i={\color{color2}1},\dots,N-1 \\
            &\bar{x}_N \in \X_f \ominus \F_N \\
            &\bar{x}_0 = x(k)
        \end{align*}
    \end{highlightbox}

    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {52},
            trim = {20mm, 10mm, 15mm, 15mm},
            clip
            ]{06_stochasticMPC1.pdf}
        }
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {12},
            trim = {14mm, 6mm, 13mm, 10mm},
            clip
            ]{06_SMPC1.pdf}
        }
    \end{minipage}

    \blue{CL Chance Constraint Satisfaction}
    \begin{whitebox}{}
        \scriptsize 
        Desired CL chance constraints 
        \begin{align*}
            (*)\quad \mathrm{Pr}\{x(k)\in\X \mid x(0)\} \geq p \qquad \forall k \geq 0
        \end{align*}
        But MPC enforces 
        \begin{align*}
            (**)\quad \mathrm{Pr}\{x({\color{color2}k+1})\in\X \mid x({\color{color2}k})\} \geq p \qquad \forall k \geq 0
        \end{align*}
        It is obvious that $(**) \Rightarrow (*)$
        \begin{align*}
            &\mathrm{Pr}\{x({\color{color2}k+1})\in\X \mid x({\color{color2}0})\} 
            = \\
            &\textstyle\int 
            \underbrace{\mathrm{Pr}\{x({\color{color2}k+1})\in\X \mid x({\color{color2}k})\}}_{\geq p} 
            \mathrm{Pr}\{x({\color{color2}k})\in\X \mid x({\color{color2}0})\} 
            dx(k) \geq p
        \end{align*}
    \end{whitebox}

    \blue{Properties}
    \begin{whitebox}{}
        \scriptsize 
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Recursive feasibility properties follow from robust case 
            \item[\textbf{+}] Tractable for polytopic $\W$ \& 1/2-space chance constraints (or collection thereof)
            \item[\textbf{+}] Asymptotic average performance directly applicable
        \end{itemize}
    \end{whitebox}
    
\end{whitebox}

\begin{whitebox}{\textbf{OPEN-LOOP SMPC}}
    \blue{Tractable Formulation} {\scriptsize\textbf{QUADRATIC COST}}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            \min_{\bar{u}_i} \ &\textstyle\sum_{i=0}^{N-1} \|\bar{x}_i\|_Q^2 + \|\bar{u}_i\|_R^2 + \|\bar{x}_N\|_P^2 \\
            \mathrm{s.t.} \ &\bar{x}_{i+1} = A\bar{x}_i + B\bar{u}_i \\
            &\Sigma_{i+1}^x = (A+BK)\Sigma_i^x (A+BK)^\top + \Sigma^w \\
            &h_{x,j}^\top \bar{x}_i \leq b_{x,j} - \sqrt{h_{x,j}^\top \Sigma_i^x h_{x,j}} \phi^{-1}(p_{x,j}), \quad \forall j=1,\dots,N_{c,x} \\
            &h_{u,j}^\top \bar{u}_i \leq b_{u,j} - \sqrt{h_{u,j}^\top K \Sigma_i^x  K^\top h_{u,j}} \phi^{-1}(p_{u,j}), \quad \forall j=1,\dots,N_{c,u} \\
            &\bar{x}_N \in \bar{\X}_f \\
            &{\color{color2}\bar{x}_0 = \bar{x}_{1|k-1}}, \qquad {\color{color2}\Sigma_0^x = \Sigma_{1|k-1}^x}
        \end{align*}
    \end{highlightbox}
    \blue{Properties}
    \begin{whitebox}{}
        \scriptsize 
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] Not reseting distributions $\leadsto$ recursively feasible, satisfies CL chance constraints
            \item[\textbf{--}] \textbf{Entirely missing FB} nowhere using true $x(k)$
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{RECOVERY-INITIALIZATION}} 
    \blue{Noise} Unimodal

    \blue{PRS -- Probabalistic Reachable Sets}
    \begin{highlightbox}{}
        $\F_i^p$ is i-step PRS for process $\{d(k)\}$ initialized at $d(0)$ if 
        \begin{align*}
            \mathrm{Pr}\{ d(i)\in\F_i^p \mid d(0) \} \geq p
        \end{align*}
    \end{highlightbox}

    \blue{Recovery Initialization With PRS}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            \min_{\bar{u}_i} \ &\textstyle\sum_{i=0}^{N-1} \|\bar{x}_i\|_Q^2 + \|\bar{u}_i\|_R^2 + \|\bar{x}_N\|_P^2 \\
            \mathrm{s.t.} \ &\bar{x}_{i+1} = A\bar{x}_i + B\bar{u}_i \\
            &\bar{x}_i \in \X \ {\color{color2}\ominus \ \F_{i+k}^p} \\
            &\bar{x}_N \in \mathcal{Z}_f \subset \X \ {\color{color2}\ominus \F_\infty^p} \\
            &{\color{color2}\bar{x}_0 = \begin{cases}
                x(k) & \text{if feasible} \\
                \bar{x}_{1|k-1} & \text{otherwise}
            \end{cases}
            }
        \end{align*}
    \end{highlightbox}
    \blue{Properties}
    \begin{whitebox}{}
        \scriptsize 
        \begin{itemize}[leftmargin=1.5em]
            \item[\textbf{+}] 
            \item[\textbf{--}] \textbf{No direct guarantees on CL constraint satisfaction}
            \item[\textbf{--}] Can be conservative! 
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{INDIRECT FEEDBACK}}
    \blue{Noise} any
    
    \blue{MPC Formulation}
    \begin{highlightbox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{alignat*}{2}
            \min_{\bar{u}_i} \ &\textstyle\sum_{i=0}^{N-1} \|\bar{x}_i\|_Q^2 + \|\bar{u}_i\|_R^2 + \|\bar{x}_N\|_P^2 \\
            \mathrm{s.t.} \ &\bar{x}_{i+1} = A\bar{x}_i + B\bar{u}_i \quad && \text{uncertain for performance}\\
            &{\color{color2} z_{i+1} = A z_i + B v_i} && {\color{color2}\text{nominal for safety}} \\
            &\bar{e}_i = \bar{x}_i - z_i \\
            &{\color{color2} z_i \in \X \ominus \ \F_{i+k}^p}  && \\
            &{\color{color2} z_N \in \mathcal{Z}_f \subset \X \ominus \F_\infty^p} \\
            &\bar{x}_0 = x(k), \quad {\color{color2} z_0 = z(k) = z_{1|k-1}}
        \end{alignat*}
    \end{highlightbox}
\end{whitebox}